var game = new Phaser.Game(this.window.innerWidth, this.window.innerHeight, Phaser.Canvas);

var style = {fill: "#2D468B"};
var balance = 1000;
var bet = 10;
var reelWidth = 90;
var slotHeight = 80;

var GameState = {
    preload: function () {
        // background
        this.load.image('backgroundPicture', 'assets/images/game-background.jpg');

        // reel
        this.load.image('reel', 'assets/images/reel.png');

        // menu bar objects
        this.load.image('autoPlay', 'assets/images/autoplay-normal.png');
        this.load.image('backButton', 'assets/images/back-normal.png');
        this.load.image('gameLogo', 'assets/images/game-logo.png');
        this.load.image('info', 'assets/images/info-normal.png');
        this.load.image('minus', 'assets/images/minus-normal.png');
        this.load.image('menuButton', 'assets/images/payTable-normal.png');
        this.load.image('plus', 'assets/images/plus-normal.png');
        this.load.image('quickSpin', 'assets/images/quickspin-normal.png');
        this.load.image('sound', 'assets/images/sound-normal.png');
        this.load.image('spin', 'assets/images/spin-normal.png');

    },
    create: function () {
        // set backgound
        this.background = game.add.sprite(0, 0, 'backgroundPicture');
        // fit background to screen
        this.background.width = window.innerWidth;
        this.background.height = window.innerHeight;

        this.centerX = game.world.centerX;
        this.centerY = game.world.centerY;

        // a mask for the slot reel
        this.graphics = game.add.graphics();
        this.graphics.beginFill(0xff0000);
        this.graphics.drawRect(0, 0, 450, 240);
        this.graphics.endFill();

        // spin reels
        this.reels = game.add.group();
        for (i = 0; i < 5; i++) {
            var reel = game.add.sprite(i * reelWidth, 0, 'reel');
            reel.width = reelWidth;
            reel.height = slotHeight * 15;
            this.reels.add(reel);
        }

        this.reels.x = this.centerX - this.reels.width / 2;
        this.reels.y = this.centerY - this.reels.height / 2 + 320;

        this.graphics.x = this.centerX - 225;
        this.graphics.y = this.centerY - 120;
        this.reels.mask = this.graphics;


        // menu bar
        this.menuBar = game.add.graphics();
        this.menuBar.beginFill(0x8CCAD7, 0.8);
        this.menuBar.drawRect(0, this.centerY + 135, window.innerWidth, 80);
        this.menuBar.endFill();


        // menu bar buttons
        this.autoPlay = game.add.sprite(this.menuBar.centerX + 180, this.centerY + 175, 'autoPlay');
        this.autoPlayText = game.add.text(this.menuBar.centerX + 180, this.centerY + 177, "AUTOPLAY", style);
        resize(this.autoPlay, 0.3);
        resize(this.autoPlayText, 0.5);

        this.spinButton = game.add.sprite(this.centerX + 280, this.centerY + 140, 'spin');
        resize(this.spinButton, 0.35);
        this.spinButton.inputEnabled = true;
        this.spinButton.events.onInputDown.add(this.start, this);


        // menu bar small buttons
        this.plus = game.add.sprite(this.centerX + 40, this.centerY + 155, 'plus');
        resize(this.plus, 0.35);

        this.minus = game.add.sprite(this.centerX + 40, this.centerY + 190, 'minus');
        resize(this.minus, 0.35);

        this.menuButton = game.add.sprite(this.centerX - 220, this.centerY + 155, 'menuButton');
        resize(this.menuButton, 0.35);

        this.infoButton = game.add.sprite(this.centerX - 220, this.centerY + 190, 'info');
        resize(this.infoButton, 0.35);

        this.quickSpinButton = game.add.sprite(this.centerX - 190, this.centerY + 190, 'quickSpin');
        resize(this.quickSpinButton, 0.35);

        this.soundButton = game.add.sprite(this.centerX - 190, this.centerY +155, 'sound');
        resize(this.soundButton, 0.35);


        // back button
        this.backButton = game.add.sprite(this.centerX - 270, this.centerY - 140, 'backButton');
        resize(this.backButton, 0.35);


        // logo
        gameLogo = game.add.sprite(this.centerX, this.centerY - 170, 'gameLogo');
        resize(gameLogo, 0.5);


        // balance text and sum
        this.balanceText = game.add.text(this.centerX - 135, this.centerY + 193, "BALANCE", style);
        resize(this.balanceText, 0.5);

        this.balanceTotal = game.add.text(this.centerX - 50, this.centerY + 193, "£" + balance.toLocaleString(), style);
        this.balanceTotal.scale.setTo(0.5);
        this.balanceTotal.anchor.setTo(1, 0.5);


        //bet text and sum
        this.betText = game.add.text(this.centerX - 10, this.centerY + 155, "Total Bet", style);
        this.betSum = game.add.text(this.centerX - 10, this.centerY + 193, "£" + bet.toLocaleString(), style);
        this.betSum.anchor.setTo(1, 0.5);
        resize(this.betText, 0.5);
        resize(this.betSum, 0.5);

        // bet change
        this.plus.inputEnabled = true;
        this.minus.inputEnabled = true;
        this.plus.events.onInputDown.add(this.increaseBet);
        this.minus.events.onInputDown.add(this.decreaseBet);

        // reel position on startup
        this.setReel(0, 0);
        this.setReel(1, 2);
        this.setReel(2, 7);
        this.setReel(3, 9);
        this.setReel(4, 4);



    },
    increaseBet: function() {
        bet += 10;
    },
    decreaseBet: function() {
        bet -= 10;
    },
    setReel: function(i, pos) {
        var reel = this.reels.getChildAt(i);
        reel.y = -(pos - 1) * slotHeight;
    },
    start: function() {
        if (bet <= balance && balance > 0) {
            balance -= bet;
            this.startSpin();
        } else if (balance > 0) {
            alert("You can't bet what You don't have");
        } else {
            this.spinButton.inputEnabled = false;
            this.minus.inputEnabled = false;
            this.plus.inputEnabled = false;
            alert("You have no money left \n Come back later");
        }
    },
    startSpin: function() {
        this.spinCount=2;

        var s1 = game.rnd.integerInRange(1, 10);
        var s2 = game.rnd.integerInRange(1, 10);
        var s3 = game.rnd.integerInRange(1, 10);
        var s4 = game.rnd.integerInRange(1, 10);
        var s5 = game.rnd.integerInRange(1, 10);

        this.setStop(0, s1);
        this.setStop(1, s2);
        this.setStop(2, s3);
        this.setStop(3, s4);
        this.setStop(4, s5);

        this.spinTimer = game.time.events.loop( Phaser.Timer.SECOND / 1000 , this.spin, this);
    },
    setStop: function(i, stopPoint) {
        var reel = this.reels.getChildAt(i);
        reel.stopPoint = stopPoint;
        reel.active=true;
        reel.spins = game.rnd.integerInRange(3, 10);
    },
    spin: function() {
        this.reels.forEach(function(reel) {
            if (reel.active == true) {
                reel.y -= reel.spins * 8;
                if (reel.y < - (slotHeight * 10)) {
                    reel.y = 0;
                    reel.spins--;
                    if (reel.spins == 0) {
                        reel.active = false;
                        this.finalSpin(reel);
                    }
                }
            }
        }.bind(this));
    },
    finalSpin: function(reel) {
        var ty = -(reel.stopPoint - 1) * slotHeight;
        var finalTween = game.add.tween(reel).to({
            y: ty
        }, 2000, Phaser.Easing.Linear.None, true);
        finalTween.onComplete.add(this.checkFinished, this);
    },
    checkFinished() {
        this.spinCount--;
        if (this.spinCount == 0) {
            game.time.events.remove(this.spinTimer);
        }
    },
    update: function () {
        this.balanceTotal.setText( "£" + balance.toLocaleString());
        this.betSum.setText("£" + bet.toLocaleString());

        if (balance <= 0) {
            this.spinButton.inputEnabled = false;
        }
    }
};

function resize(sprite, scale) {
    sprite.anchor.setTo(0.5);
    sprite.scale.setTo(scale);
}

game.state.add('GameState', GameState);
game.state.start('GameState');